﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Weather.Api.Models.Weather;
using static Weather.Api.Models.Shared.CacheDuration;
namespace Weather.Api.Controllers
{
    [Route("api/weather")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IWeatherModelBuilder modelBuilder;

        public WeatherController(IWeatherModelBuilder modelBuilder)
        {
            this.modelBuilder = modelBuilder;
        }

        [ResponseCache(Duration = _30_MIN, Location = ResponseCacheLocation.Any, VaryByQueryKeys = new[] {"country"})]
        [HttpGet("{city}")]
        public async Task<ActionResult<WeatherModel>> Get(string city, string country)
            => await modelBuilder.Build(city, country);
    }
}