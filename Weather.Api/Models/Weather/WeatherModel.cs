using System;
using Newtonsoft.Json;

namespace Weather.Api.Models.Weather
{
    public class WeatherModel
    {
        public WeatherModel(
            decimal temperatureCelsius, 
            decimal humidity, 
            decimal pressureMmHg,
            Wind wind, 
            DateTimeOffset sunriseUtc, 
            DateTimeOffset sunsetUtc)
        {
            TemperatureCelsius = temperatureCelsius;
            Wind = wind;
            SunriseUtc = sunriseUtc;
            SunsetUtc = sunsetUtc;
            Humidity = humidity;
            PressureMmHg = pressureMmHg;
        }

        [JsonProperty("temperatureCelsius")]
        public decimal TemperatureCelsius { get; }
        
        [JsonProperty("humidity")]
        public decimal Humidity { get; }
        
              
        [JsonProperty("pressureMmHg")]
        public decimal PressureMmHg { get; }
        
        [JsonProperty("wind")]
        public Wind Wind { get; }
        [JsonProperty("sunriseUtc")]
        public DateTimeOffset SunriseUtc { get; }
        [JsonProperty("sunsetUtc")]
        public DateTimeOffset SunsetUtc { get; }
    }
}