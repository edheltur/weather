using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Weather.Api.Models.Shared;

namespace Weather.Api.Models.Weather
{
    public class CachedWeatherModelBuilder : IWeatherModelBuilder
    {
        private readonly WeatherModelBuilder weatherModelBuilder;
        private readonly IMemoryCache memoryCache;

        public CachedWeatherModelBuilder(WeatherModelBuilder weatherModelBuilder, IMemoryCache memoryCache)
        {
            this.weatherModelBuilder = weatherModelBuilder;
            this.memoryCache = memoryCache;
        }

        public async Task<WeatherModel> Build(string city, string country)
        {
            var key = $"{nameof(CachedWeatherModelBuilder)}_{city}_{country}";

            Task<WeatherModel> Build(ICacheEntry _)
                => weatherModelBuilder.Build(city, country);

            return await memoryCache.GetOrCreateAsync(key, Build);
        }
    }
}