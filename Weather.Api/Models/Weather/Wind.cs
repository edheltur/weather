using Newtonsoft.Json;

namespace Weather.Api.Models.Weather
{
    public class Wind
    {
        public Wind(decimal speedMps, decimal headingDeg)
        {
            SpeedMps = speedMps;
            HeadingDeg = headingDeg;
        }

        [JsonProperty("speedMps")]
        public decimal SpeedMps { get; }
        [JsonProperty("headingDeg")]
        public decimal HeadingDeg { get; }
    }
}