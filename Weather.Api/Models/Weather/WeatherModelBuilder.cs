using System;
using System.Threading.Tasks;
using Weather.Api.Models.Weather.OpenWeather;

namespace Weather.Api.Models.Weather
{
    public class WeatherModelBuilder : IWeatherModelBuilder
    {
        private readonly IOpenWeatherClient apiClient;

        public WeatherModelBuilder(IOpenWeatherClient apiClient)
        {
            this.apiClient = apiClient;
        }

        public async Task<WeatherModel> Build(string city, string country)
        {
            var response = await apiClient.GetWeather(city, country);
            var main = response.Main;
            var wind = response.Wind;
            var sys = response.Sys;
            
            return new WeatherModel(
                temperatureCelsius: KelvinToCelsius(main.Temp),
                humidity: main.Humidity,
                pressureMmHg: main.Pressure,
                wind: new Wind(speedMps: wind.Speed, headingDeg: wind.Deg),
                sunriseUtc: DateTimeOffset.FromUnixTimeSeconds(sys.SunriseTimestamp),
                sunsetUtc: DateTimeOffset.FromUnixTimeSeconds(sys.SunsetTimestamp)
            );
        }

        private static decimal KelvinToCelsius(decimal kelvin) =>
            kelvin - 273;
    }
}