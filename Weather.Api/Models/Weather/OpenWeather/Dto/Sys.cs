using Newtonsoft.Json;

namespace Weather.Api.Models.Weather.OpenWeather.Dto
{
    public class Sys
    {
        [JsonProperty("sunrise")]
        public int SunriseTimestamp { get; set; }

        [JsonProperty("sunset")]
        public int SunsetTimestamp { get; set; }
    }
}