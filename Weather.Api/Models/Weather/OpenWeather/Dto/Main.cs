using Newtonsoft.Json;

namespace Weather.Api.Models.Weather.OpenWeather.Dto
{
    public class Main
    {

        [JsonProperty("temp")]
        public decimal Temp { get; set; }

        [JsonProperty("pressure")]
        public decimal Pressure { get; set; }

        [JsonProperty("humidity")]
        public decimal Humidity { get; set; }

    }
}