using System.Collections.Generic;
using Newtonsoft.Json;

namespace Weather.Api.Models.Weather.OpenWeather.Dto
{
    public class WeatherResponse
    {

        [JsonProperty("main")]
        public Main Main { get; set; }

      
        [JsonProperty("wind")]
        public Wind Wind { get; set; }

        [JsonProperty("sys")]
        public Sys Sys { get; set; }

    }
}