using Newtonsoft.Json;

namespace Weather.Api.Models.Weather.OpenWeather.Dto
{
    public class Wind
    {

        [JsonProperty("speed")]
        public decimal Speed { get; set; }

        [JsonProperty("deg")]
        public decimal Deg { get; set; }
    }
}