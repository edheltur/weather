using System;
using JetBrains.Annotations;

namespace Weather.Api.Models.Weather.OpenWeather
{
    public static class QueryBuilder
    {
        public static Uri BuildWeatherUri(
            [NotNull] this OpenWeatherOptions options,
            [NotNull] string city,
            [CanBeNull] string country = null)
        {
            var query = !string.IsNullOrEmpty(country)
                ? $"{city},{country}"
                : city;

            var builder = new UriBuilder(options.BaseAddress);
            builder.Path += "/weather";
            builder.Query = $"q={query}&appid={options.ApiKey}";

            return builder.Uri;
        }
    }
}