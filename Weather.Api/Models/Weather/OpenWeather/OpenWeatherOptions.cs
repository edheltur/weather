using System;
using JetBrains.Annotations;

namespace Weather.Api.Models.Weather.OpenWeather
{
    [UsedImplicitly(ImplicitUseKindFlags.Assign, ImplicitUseTargetFlags.WithMembers)]
    public class OpenWeatherOptions
    {
        public Uri BaseAddress { get; set; } =
            new Uri("https://api.openweathermap.org/data/2.5");

        public string ApiKey { get; set; }
    }
}