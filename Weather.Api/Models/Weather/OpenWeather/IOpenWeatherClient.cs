using System.Threading.Tasks;
using JetBrains.Annotations;
using Weather.Api.Models.Weather.OpenWeather.Dto;

namespace Weather.Api.Models.Weather.OpenWeather
{
    public interface IOpenWeatherClient
    {
        Task<WeatherResponse> GetWeather([NotNull] string city, [CanBeNull] string country);
    }
}