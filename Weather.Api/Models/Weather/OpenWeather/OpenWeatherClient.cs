using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Weather.Api.Models.Shared;
using Weather.Api.Models.Shared.Exceptions;
using Weather.Api.Models.Weather.OpenWeather.Dto;

namespace Weather.Api.Models.Weather.OpenWeather
{
    public class OpenWeatherClient : IOpenWeatherClient
    {
        private readonly HttpClient httpClient;
        private readonly OpenWeatherOptions options;

        public OpenWeatherClient(HttpClient httpClient,
            IOptionsSnapshot<OpenWeatherOptions> optionsSnapshot)
        {
            this.httpClient = httpClient;
            this.options = optionsSnapshot.Value;
        }

        public async Task<WeatherResponse> GetWeather(string city, string country)
        {
            var requestUri = options.BuildWeatherUri(city, country);
            var response = await httpClient
                .GetAsync(requestUri)
                .ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new HttpStatusException(HttpStatusCode.NotFound, "Unknown City");

            response.EnsureSuccessStatusCode();

            return await response.Content
                .ReadAsAsync<WeatherResponse>()
                .ConfigureAwait(false);
        }
    }
}