using System.Threading.Tasks;

namespace Weather.Api.Models.Weather
{
    public interface IWeatherModelBuilder
    {
        Task<WeatherModel> Build(string city, string country);
    }
}