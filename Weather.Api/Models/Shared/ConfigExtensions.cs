using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Weather.Api.Models.Shared
{
    public static class ConfigExtensions
    {
        public static string[] GetCorsDomains(this IConfiguration config) =>
            config
                .GetSection(ConfigNames.CORS_DOMAINS)
                .AsEnumerable()
                .Select(x => x.Value)
                .ToArray();
    }
}