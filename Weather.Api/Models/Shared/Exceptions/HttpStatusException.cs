using System;
using System.Net;

namespace Weather.Api.Models.Shared.Exceptions
{
    public class HttpStatusException : Exception
    {
        public HttpStatusException(HttpStatusCode code, string message = null)
            : base(message)
        {
            Code = code;
        }

        public HttpStatusCode Code { get; }
    }
}