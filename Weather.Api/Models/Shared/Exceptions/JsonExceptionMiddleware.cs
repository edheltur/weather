using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Weather.Api.Models.Shared.Exceptions
{
    public class JsonExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public JsonExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        [UsedImplicitly]
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                response.StatusCode = exception is HttpStatusException statusException
                    ? (int) statusException.Code
                    : (int) HttpStatusCode.InternalServerError;


                using (var writer = new StreamWriter(response.Body))
                {
                    var error = new
                    {
                        code = response.StatusCode,
                        message = exception.Message
                    };
                    new JsonSerializer().Serialize(writer, error);
                    await writer.FlushAsync().ConfigureAwait(false);
                }
            }
        }
    }
}