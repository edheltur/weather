namespace Weather.Api.Models.Shared
{
    public static class CacheDuration
    {
        public const int _30_MIN = 30 * 60;
    }
}