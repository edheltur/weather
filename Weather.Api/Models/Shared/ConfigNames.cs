namespace Weather.Api.Models.Shared
{
    public static  class ConfigNames
    {
        public const string OPEN_WEATHER = "OPEN_WEATHER";
        public const string CORS_DOMAINS = "CORS_DOMAINS";
    }
}