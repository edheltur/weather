﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Weather.Api.Models.Shared;
using Weather.Api.Models.Shared.Exceptions;
using Weather.Api.Models.Weather;
using Weather.Api.Models.Weather.OpenWeather;

namespace Weather.Api
{
    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class Startup
    {
        private readonly IConfiguration config;

        public Startup(IConfiguration config)
        {
            this.config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddSingleton<JsonExceptionMiddleware>()
                .AddScoped<WeatherModelBuilder>()
                .AddScoped<IWeatherModelBuilder, CachedWeatherModelBuilder>();

            services.Configure<OpenWeatherOptions>(
                config.GetSection(ConfigNames.OPEN_WEATHER));

            services.AddHttpClient<IOpenWeatherClient, OpenWeatherClient>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5));

            services
                .AddResponseCaching()
                .AddMemoryCache();

            services.AddCors(options => options
                .AddDefaultPolicy(builder => builder
                    .WithOrigins(config.GetCorsDomains())
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                ));

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<JsonExceptionMiddleware>();
            app.UseResponseCaching();
            app.UseCors();
            app.UseMvc();
        }
    }
}