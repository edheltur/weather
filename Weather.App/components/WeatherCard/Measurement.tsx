import * as React from "react";
import styles from './measurements.css'
type MeasurementProps = {
    title: string,
    children: React.ReactNode
}

export function Measurement({title, children}: MeasurementProps) {
    return <>
        <dd className={styles.measurements__title}>{title}</dd>
        <dt className={styles.measurements__value}>{children}</dt>
    </>
}