import * as React from "react";
import {Card, Icon, Flag, FlagNameValues} from "semantic-ui-react";
import {Measurement} from "./Measurement";
import styles from './measurements.css'
import {WeatherInfo} from "../../models/WeatherInfo";


export function WeatherCard({city, weather}: WeatherInfo) {
    const header = <><Icon name='location arrow'/>{city.name}</>;
    return <Card as={'section'}>
        <Card.Content header={header}/>
        <Card.Content as={'dl'} className={styles.measurements}>
            <Measurement title='Temperature'>
                {weather.temperatureCelsius}°C
            </Measurement>

            <Measurement title='Humidity'>
                {weather.humidity}%
            </Measurement>

            <Measurement title='Pressure'>
                {weather.pressureMmHg} mm Hg
            </Measurement>

            <Measurement title='Wind'>
                {weather.wind.speedMps} mps
            </Measurement>
        </Card.Content>
        <Card.Content extra>
            <Flag name={city.country as FlagNameValues}/>
            {city.country.toUpperCase()}
        </Card.Content>
    </Card>
}