const withCSS = require('next-typed-css');

module.exports = withCSS({
    publicRuntimeConfig: {
        API_URL: process.env.API_URL
    },
    tsCssModules: true,
    cssLoaderOptions: {
        namedExport: true
    }
});