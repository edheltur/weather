import * as React from 'react';
import {WeatherCard} from '../components/WeatherCard';
import Head from "next/head";
import {CardsFetcher} from '../models/CardsFetcher';
import {WeatherInfo} from "../models/WeatherInfo";
import {Card} from 'semantic-ui-react';
import '../styles/global.css'

type OwnProps = {
    cards: WeatherInfo[]
}

class Index extends React.Component<OwnProps> {
    static async getInitialProps(): Promise<OwnProps> {
        const cities = [
            {name: 'London', country: 'uk'},
            {name: 'Paris', country: 'fr'},
            {name: 'Saint Petersburg', country: 'ru'},
            {name: 'Stockholm', country: 'se'}
        ];
        return {cards: await CardsFetcher.fetch(cities)}
    }

    render() {
        const {cards} = this.props;
        return <>
            <Head>
                <title>Weather</title>
                <link
                    rel="stylesheet"
                    href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"
                />
            </Head>
            <Card.Group stackable centered as="main">
                {cards.map(({city, weather}) =>
                    <WeatherCard key={`${city.name}_${city.country}`}
                                 city={city}
                                 weather={weather}/>)}
            </Card.Group>
        </>;
    }
}

export default Index;