export type Weather = {
    temperatureCelsius: number,
    humidity: number,
    pressureMmHg: number,
    wind: Wind,
    sunriseUtc: Date,
    sunsetUtc: Date
}

export type Wind = {
    speedMps: number,
    headingDeg: number
}