import fetch from "isomorphic-unfetch";
import {City} from "./City";
import {WeatherInfo} from "./WeatherInfo";
import getConfig from 'next/config'

const {publicRuntimeConfig} = getConfig();
const {API_URL} = publicRuntimeConfig;


export class CardsFetcher {
    static async fetch(cities: City[]): Promise<WeatherInfo[]> {
        
        const cardPromises = cities.map(
            async city => {
                const response = await fetch(`${API_URL}/api/weather/${city.name}?country=${city.country}`);
                const weather = await response.json();
                return {city, weather};
            });
        return Promise.all(cardPromises);

    }
}