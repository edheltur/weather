import {City} from "./City";
import {Weather} from "./Weather";

export interface WeatherInfo {
    city: City,
    weather: Weather
}